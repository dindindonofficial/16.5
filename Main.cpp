#include <iostream>
#include <time.h>


int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	int sum = 0;

	const int n = 2;
	int array[n][n] = { {1,2},{3,4} };

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			std::cout << array[i][j];
		}
		std::cout << '\n';
	}

	int indecs = buf.tm_mday % n;
	for (int j = 0; j < n; j++)
	{
		sum = sum + array[indecs][j];
	}
	std::cout << sum;
}

